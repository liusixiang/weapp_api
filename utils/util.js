/**
 * 弹出一个 1.5s的 type类型、内容为msg的提醒
 * type为空 自动显示成功提示
 * @param {*} msg 
 * @param {*} type 
 */
function toast(msg,type){
    wx.showToast({
      title: msg,
      mask: true,
      icon: type,
      duration: 1500,
      mask: true
    });
}

/**
 * 判断值是否为空
 * @param {*} val 
 */
function judgeNull(T){
  var val = false;
  if (T == null || T == undefined|| T === '') {
    val = true;
  }
  return val;
}

module.exports = {
  toast,judgeNull
}
