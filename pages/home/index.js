// pages/home/index.js
Page({

      /**
       * 页面的初始数据
       */
      data: {

      },
      // 唯一点击事件 界面跳转
      handleClick: function(e){
         console.table(e)
         const id = Number(e.currentTarget.dataset.id);
         switch (id) {
            case 1:
              wx.navigateTo({url: "/pages/index/index"});   // 图片-模拟器
            break;
            case 2:
              wx.navigateTo({url: "/pages/player/index"});  // 音乐播放器
            break;
            case 3:
              wx.navigateTo({url: "/pages/article/index"});  // 查看公众号文章
            break;
            case 4:
              wx.navigateTo({url: "/pages/selectInformation/index"});  // 头像昵称填写
            break;
            case 5:
              wx.navigateTo({url: "/pages/map/index"});  // 地图api
            break;
         
            default:
            break;
         }
      },
})