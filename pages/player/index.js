// 创建音频播放实例
const myAudio = wx.createInnerAudioContext();
import Utils from '../../utils/util.js';
import * as Storage from '../../storage/storage.js';
Page({
      /**
       * 页面的初始数据
       */
      data: {
        songList:[
           {
            id: 1,name: "那些年-胡夏(本地)",
            img:'/../resour/img/thoseYearsImg.jpg',
            src:'resour/audio/song-thoseYears.mp3'
           },
           {
            id: 2,name: "表态-梁博(网址)",
            img:'/../resour/img/hasStandImg.png',
            src:'https://freetyst.nf.migu.cn/public/product9th/product44/2021/05/3123/2021%E5%B9%B405%E6%9C%8831%E6%97%A519%E7%82%B956%E5%88%86%E7%B4%A7%E6%80%A5%E5%86%85%E5%AE%B9%E5%87%86%E5%85%A5%E4%B8%AD%E5%8A%B2%E8%BF%9C%E4%B8%9C1%E9%A6%96/%E5%85%A8%E6%9B%B2%E8%AF%95%E5%90%AC/Mp3_64_22_16/69906714936231240.mp3'
           },
           {
            id: 3,name: "你走-(本地)",
            img:'/../resour/img/youGoImg.jpg',
            src:'resour/audio/song-youGo.mp3'
           },
        ],

            // 音频播放控制
            audioDisplay: null,     // 播放音乐进度条
            forNowTime: '0',  // 当前进度时间
            forAllTime: '0',  // 歌曲总时长
            duration: 0,      // 总时间 秒
            current: 0,      // slider当前进度
            seek: false,  // 是否处于拖动状态
            paused: true, // 是否处于暂停状态
            doubleSpeed: 1.0, //播放倍速 默认1.0
            doubleSpeedSet: false, //倍速设置小弹窗

            // 当前正在播放歌曲信息
            noneImg: "/../resour/icon/cover-none.png",
            songIndex: null,  // 整在播放歌曲下标

            isUpload: false,  // 隐藏框

            // 待上传歌曲信息
            songName:'',
            songImg:'',
            songUrl:'',
      },

      /**
       * 生命周期函数--监听页面加载
       */
      onLoad: function (options) {
         this.audioInit();
         const newSong = wx.getStorageSync(Storage.SONG_LIST);
         console.log('缓存中的歌曲-----',newSong)
         if(newSong.length != 0){
               this.loadSongs();
         }
         
      },

      audioInit: function() {
            const that = this;
            myAudio.playbackRate = 1.0;   // 此处若不设置，页面上点击设置倍速就不会产生效果
            myAudio.onPlay(() => {
                  console.log('开始播放')
                  console.log('播放速度-',myAudio.playbackRate);
            });
            myAudio.onPause(() => {
                  console.log('停止播放')
            });
            // 监听音频进入可以播放状态的事件。但不保证后面可以流畅播放，
            // 必须要这个监听，不然播放时长更新监听不会生效，不能给进度条更新值
            myAudio.onCanplay(() => {
                  myAudio.duration
            });

            // 播放时长更新监听
            myAudio.onTimeUpdate(() => {
                  that.setData({
                        forNowTime: parseInt(myAudio.currentTime),
                        forAllTime: parseInt(myAudio.duration),
                        current: myAudio.currentTime,
                        duration: myAudio.duration
                  });
            });

            // 播放出错监听
            myAudio.onError((err) => {
                  console.log(err.errMsg)
                  console.log(err.errCode)
            });
      },

      // 开始播放
      audioPlay(val){
         console.log('开始播放');
         console.log(val);
         const index = Number(val.currentTarget.dataset.index)
         const id = Number(val.currentTarget.dataset.info.id)
         if(val){
            this.setData({    // 将值赋给播放器
              audioDisplay: id
            });
            this.setData({ songIndex: index  });
            // if( id === 1){
            //       console.log('用户点击了第一首歌',id,this.data.songList[id].name)
            // }else{
            //       this.setData({ songIndex: id - 1 })
            // }
            myAudio.src = val.currentTarget.dataset.info.src;
            // 定位至选中进度条的播放位置
            this.setData({ paused: false });
            // 取消暂停
            myAudio.play();
                  // 继续播放
         }
      },

      // 继续播放上一曲
      // audioContinue(){
      audioContinue(){
            const index = this.data.songIndex;
            if(index){
                  this.setData({ 
                      paused: false,
                  }); // 取消暂停
                  myAudio.src = this.data.songList[index].src;
                  myAudio.play(); // 继续播放
            }else{
                  // 可设置为从第一首歌开始播放
                  Utils.toast('未选择歌曲','none');
            }
      },
      // 切歌
      cutSong(e){
            const that = this;
            const list = this.data.songList;
            const index = this.data.songIndex;
            const val = Number(e.currentTarget.dataset.type);
           switch (val) {
                 case 1:
                  if(index == list.length-1){
                        // 列表最后一首 切换回第一首
                        that.setData({ paused: false, songIndex: 0 });
                        myAudio.src = that.data.songList[0].src;
                        myAudio.play();
                  }else{
                        that.setData({ paused: false, songIndex: index+1 });
                        myAudio.src = that.data.songList[index+1].src;
                        myAudio.play();   
                  }
                  //      下一曲
                  break;
                  case - 1:
                  if(index == 0){
                        // 第一首 切回尾曲
                        that.setData({ paused: false, songIndex: list.length-1 });
                        myAudio.src = that.data.songList[list.length-1].src;
                        myAudio.play();
                  }
                  break;
           
                 default:
                       break;
           }
      },
      // 暂定播放
      audioPause(){
            this.setData({ paused:true });
            myAudio.pause();
      },

      // 进度条实时改变
      audioChanging(val){
            myAudio.seek(val.detail.value);     //seek-改变播放实例进度
            this.setData({ forNowTime: Number(val.detail.value) });     // 界面随之改变
      },

      // 手动修改进度条
      audioChange(val){
            myAudio.seek(val.detail.value);
            // data中的值已经发生了改变 所以只需要修改进度条内容即可
      },

      // 倍速播放
      setSpeed(){
            // 单向改变为双倍
            this.setData({ doubleSpeedSet:true })
      },

      // 倍速具体值
      setSpeedTo(val){
            myAudio.playbackRate = Number(val.currentTarget.dataset.num);
            this.setData({ doubleSpeed: val.currentTarget.dataset.num,doubleSpeedSet: false });
            // 设置倍速 取消双倍速
      },

      // 关闭小窗
      setSpeedClose(){
            this.setData({doubleSpeedSet:false});
      },

      // 弹窗显示、隐藏切换
      showUpload: function(){
            const that = this;
            this.setData({isUpload: !that.data.isUpload})
      },

      // 提交选择框
      handleClick: function(){
            const that = this;
            const names = that.data.songName;
            const img = that.data.songImg;
            const url = that.data.songUrl;
            if(Utils.judgeNull(names)){
              return Utils.toast('歌名不能为空','none');
            }
            if(Utils.judgeNull(img)){
              return Utils.toast('封面不能为空','none')
            }
            if(Utils.judgeNull(url)){
              return Utils.toast('路径不能为空','none')
            }
            const oldSong = wx.getStorageSync(Storage.SONG_LIST);
            if(!oldSong){
               // 为空则直接 插入到stroage中 
               wx.setStorageSync(Storage.SONG_LIST,[{id: 4,name:names,img:img,src:url}]);
               that.loadSongs();
            }
            // console.log('插入前',oldSong)
            // oldSong = oldSong.splice(oldSong.length-1,0,{id:oldSong.length+2,name:names,img:img,src:url});
            // console.log('插入后',oldSong)
      },
      // 输入框复制
      bindKeyInput: function(e){
            const val = e.detail.value;
            const type = Number(e.currentTarget.dataset.type);
            switch (type) {
                  case 1:
                    this.setData({songName: val});
                  break;
                  case 2:
                    this.setData({songImg: val});
                  break;
                  case 3:
                    this.setData({songUrl: val});
                  break;
                  default:
                   Utils.toast('操作异常','none');
                  break;
            }
      },

      // 加载本地存储中的歌曲数据
      loadSongs(){
        const that = this;
        const stoageSongs = wx.getStorageSync(Storage.SONG_LIST);
        let songs = that.data.songList; 
        for(var i = 0;i <= stoageSongs.length-1;i++){
            console.log(stoageSongs[i]);//    splice
            songs = songs.splice(songs.length,0,stoageSongs[i])
      }
      console.log(songs)
      // that.setData({songList:list})
      //   console.log(that.data.songList)
      },

      onHide: function(){
            // 退出界面结束播放
            this.audioPause();
      },
      onUnload: function(){
            // 界面销毁 停止播放 销毁播放实例
            this.audioPause();
            myAudio.stop();
      },
})