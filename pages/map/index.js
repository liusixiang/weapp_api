import Utils from '../../utils/util'; 
var QQMapWX = require('../../utils/qqmap-wx-jssdk.js');
var qqmapsdk;
Page({
      data: {
            MapContext: {}, // 地图实例
            local: {} //经纬度
      },
      onLoad: function (options) {
            const that = this;
            // // 实例化API核心类
            // qqmapsdk = new QQMapWX({
            //     key: 'HMTBZ-G34KS-7TAO3-6I6EY-OIBTJ-GRFRQ'
            // });
            that.setData({
               MapContext: wx.createMapContext('maps',that)
            })
      },

      // 定位
      handlelocation: function(){
            const that = this;
            var local = null;
            wx.getLocation({
                  type: 'wgs84',
                  altitude: false,
                  success: (result)=>{
                        console.table(result)
                        local = { 'longitude': result.longitude, 'latitude' :result.latitude };
                        // that.setData({
                        //    local: { longitude: result.longitude, latitude :result.latitude }  
                        // });
                        console.log('用户已经进入到了success')
                        that.data.MapContext.moveToLocation(local);
                  },
                  fail: (err) => {
                        console.log(err)
                  }
            });
      },
})