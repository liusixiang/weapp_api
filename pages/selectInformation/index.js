// pages/selectInformation/index.js
Page({

      /**
       * 页面的初始数据
       */
      data: {
            uploadImg: '',
      },

      /**
       * 生命周期函数--监听页面加载
       */
      onLoad: function (options) {},

      // 头像、昵称 选择
      // 2022 年 10 月 25 日 24 时后、
      // 选择头像以及昵称 非固定
      // 使用场景 拥有完整的用户系统
      // 需要将 button 组件 open-type 的值设置为 chooseAvatar
      // 从基础库 2.21.2 开始支持
      getAvatar: function(e){
            console.log(e)
            // const { avatarUrl } = e.detail;
            // this.setData({uploadImg:avatarUrl});
      },
      // 游客测试版本 不生效
      // 真实环境已测试 - 使用通过
      // 会调起 使用微信头像+头像图片|从相册选择|拍照|取消 
      // 用户点击选项之后 才会触发choose方法
      // 点击(微信头像选项 返回) 一个在线地址(临时路径)
      // 而拍照以及选择图片 返回本地相对路径

      bindchooseavatar: function (e) {
            console.log(e)
      }

      // type="nickname"
      // 使用这个接口 获取微信用户名 或者手动输入
})