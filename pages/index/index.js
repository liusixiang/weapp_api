// index.js
// 获取应用实例
const app = getApp()

Page({
  data: {
    imgList: [],  // 图片临时存储
  },
  onload: function() {
    
  },

  // 新增图片
  // 吊起相册权限 获取图片
  addPicture: function() {
    const that = this;
    wx.chooseImage({
      count: 6,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success (res) {
        // tempFilePath可以作为img标签的src属性显示图片
        console.log(res)
        const tempFilePaths = res.tempFilePaths
        that.addjudge(tempFilePaths);
      }
    });
  },

  addjudge: function(val) {
    const that = this;
    var list = this.data.imgList;
    if(list.length == 0){
      that.setData({
        imgList: val
      }); 
    }else if(list.length < 0){
      that.setData({
        imgList: []
      });
    }else{
      // 第二次新增时 判断原列表中的值
      for(var i = 0;i <= (6 - list.length);i++){
        list.push(val[i])
      };
      that.setData({
        imgList: list
      });
      wx.showToast({
        title: '已超限',
        icon: 'none',
        duration: 1500,
        mask: true,
      });
    }
  },

  // 用户查看图片
  checkImg: function(e) {
    const tfp = e.currentTarget.dataset.val;
    wx.previewImage({
      current: tfp, // 当前显示图片的http链接
      urls: this.data.imgList, // 需要预览的图片http链接列表 - 自动排序
      showmenu: true,

    })
  },

  // 长按删除
  deleteImg: function(e) {
    const index = e.currentTarget.dataset.val;
    var list = this.data.imgList;
    list.splice(index, 1);
    this.setData({
      imgList: list
    });
  },
})
