/**
 * 本地存储变量定义
 * 
 * Create by distance 2022年5月14日
 * 
 * 存|setStorageSync、取|getStorageSync、删|removeStorageSync
 */

 // 音乐播放器本地存储列表
 export const SONG_LIST = 'song_list';